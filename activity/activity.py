# The goal of the activity is to solve simple logical problems using Python’s control structures

# 1. Accept a year input from the user and determine if it is a leap year or not.
year = int(input("Please input a year\n"))
isLeap = year%4
if isLeap != 0:
	print(str(year) + " is not a leap year")
else:
	print(str(year) + " is a leap year")
# 2. Accept a year input from the user and determine if it is a leap year or not.
rows = int(input("Enter number of rows\n"))
columns = int(input("Enter number of columns\n"))
for x in range(0,rows):
	y = 0
	while y < columns:
		print(" *", end = "")
		y+=1
	print("\n")

# [STRETCH] Add a validation for the leap year input:
# a. Strings are not allowed for inputs
# b. No zero or negative values
i = 1
while i != 0:
	userInput = input("Please input a year\n")
	try:
		check = int(userInput)
	except ValueError:
		print("Your input is not a number. Try again")
		continue
	isLeap = year%4
	if isLeap != 0:
		print(str(year) + " is not a leap year")
	else:
		print(str(year) + " is a leap year")
	i = 0